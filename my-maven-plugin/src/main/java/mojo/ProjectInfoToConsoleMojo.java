package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

@Mojo(name = "project-info-to-console")
public class ProjectInfoToConsoleMojo extends AbstractMojo {
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try(FileReader reader = new FileReader(".\\info.txt");
            Scanner scanner = new Scanner(reader)){
            String info = scanner.nextLine();
            getLog().info(info);
        } catch (FileNotFoundException e){
            getLog().info("The report is missed");
        } catch (IOException e){
            getLog().info(e.getMessage());
        }

    }
}
