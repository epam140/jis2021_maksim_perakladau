package util;


import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import java.io.File;
import java.util.Properties;

public class MailSender{
       public static void send(String sender, String password, String recipient, String emailSubject) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props, new
                jakarta.mail.Authenticator() {
                    protected PasswordAuthentication
                    getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, password);
                    }
                });
        Message msg = new MimeMessage(session);

        try {

            msg.setFrom(new InternetAddress(sender));

            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipient, false));

            msg.setSubject(emailSubject);

            MimeBodyPart p1 = new MimeBodyPart();


            MimeBodyPart p2 = new MimeBodyPart();
            File attachment = new File(".\\info.txt");
            if(attachment.exists()){
                FileDataSource fds = new FileDataSource(attachment);
                p1.setText("Project has been built");
                p2.setDataHandler(new DataHandler(fds));
                p2.setFileName(fds.getName());

                Multipart mp = new MimeMultipart();
                mp.addBodyPart(p1);
                mp.addBodyPart(p2);

                msg.setContent(mp);
            } else {
                p1.setText("The report is missed");
                Multipart mp = new MimeMultipart();
                mp.addBodyPart(p1);
                msg.setContent(mp);
            }

            Transport.send(msg);

        } catch (MessagingException e ) {
            e.printStackTrace();
        }

    }
}
