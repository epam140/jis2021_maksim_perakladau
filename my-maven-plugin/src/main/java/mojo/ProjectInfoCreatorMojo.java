package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Mojo(name = "project-info-creator", defaultPhase = LifecyclePhase.COMPILE)
public class ProjectInfoCreatorMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.groupId}")
    String groupId;

    @Parameter(defaultValue = "${project.artifactId}")
    String artifactId;

    @Parameter(defaultValue = "${project.version}")
    String version;

    @Parameter(defaultValue = "${timestamp}")
    String buildTimestamp;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File file = new File(".\\info.txt");
        try(FileWriter writer = new FileWriter(file, false)){
            writer.write(groupId + ", " + artifactId + ", " + version + ", " + buildTimestamp);
            writer.flush();
            getLog().info("Report has been created at " + buildTimestamp);
        } catch (IOException e){
            getLog().info(e.getMessage());
        }
    }
}
