package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import util.MailSender;

@Mojo(name = "project-info-sender")
public class ProjectInfoSenderMojo extends AbstractMojo {

    @Parameter(name = "username", required = true)
    String username;

    @Parameter(name = "password", required = true)
    String password;

    @Parameter(defaultValue = "${project.name}")
    String projectName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
            MailSender.send(username, password, username, projectName + " : installed successfully");
    }
}
